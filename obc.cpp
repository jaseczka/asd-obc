#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <stack>
#include <algorithm>

#ifdef DEBUG
bool debug = true;
#else
bool debug = false;
#endif

//city2, load
typedef std::vector<int> city_t;
static const int N = 500000; 

//city1, city_t it
struct node_t
{
    int depth;
    int parent;
    long long load;
    city_t cmap;
    std::vector<int> ancestor;
    node_t(): depth(-1), parent(-1), load(0) {}
};

std::vector<node_t> cities(N);

int go_up(int node, size_t up)
{
    int pow = 1;
    size_t i = 0;
    while (up) {
        if (up % 2) {
            if (cities[node].ancestor.size() <= i)
                return -1;
            node = cities[node].ancestor[i];
        }
        up /= 2;
        pow *= 2;
        ++i;
    }

    return node;
}

void init_in(int node, int parent, int depth)
{
    if (debug) std::cout << "Ustawiam węzeł: " << node << 
                    " parent: " << parent << " depth: " << depth << "\n";
    cities[node].parent = parent;
    cities[node].depth = depth;
    depth++;
    
    int pow = 1;
    int anc = parent;
    while (anc > -1) {
        if (debug) std::cout << "Dodaję przodka " << anc << "\n";
        cities[node].ancestor.push_back(anc);
        pow *= 2;
        anc = go_up(parent, pow - 1);
    }

    for (size_t i = 0; i < cities[node].cmap.size(); ++i)
        if (cities[node].cmap[i] != cities[node].parent)
            init_in(cities[node].cmap[i], node, depth);
}

void init_tree()
{
    //ustawiam korzeń = 0
    init_in(0, -1, 0);
}

void add_val_up(int node, int val)
{
    cities[node].load += val;
}

void insert(int c1, int c2, long long val)
{
    int hi, lo;
    if (cities[c1].depth < cities[c2].depth) {
        hi = c1;
        lo = c2;
    } else {
        hi = c2;
        lo = c1;
    }

    int min_depth = cities[hi].depth;

    if (debug) std::cout << "min głębokość dla " << c1 << " i " <<
                    c2 << " to: " << min_depth << "\n";
    
    lo = go_up(lo, cities[lo].depth - cities[hi].depth);

    if (debug) std::cout << "dotarliśmy do tej samej wysokości, "
                    "lo: " << lo << " gł. " << cities[lo].depth <<
                    " hi: " << hi << " gł. " << cities[hi].depth << "\n";

    while (lo != hi) {
        if (debug) std::cout << "wstawiamy wartości dla " << lo << 
                        " i " << hi << " oraz ich rodziców\n";
        lo = cities[lo].parent;
        hi = cities[hi].parent;
    }
    
    //trasa1 od c1 do lo/hi i trasa2 od c2 do lo/hi
    add_val_up(c1, val);
    add_val_up(c2, val);
    add_val_up(hi, -2*val); 
}

void max_load(int node, long long& load, long long& max)
{
    long long load_son, max_son;
    max = 0;
    load = 0;
    for (size_t i = 0; i < cities[node].cmap.size(); ++i) {
        if (cities[node].parent != cities[node].cmap[i]) {
            max_load(cities[node].cmap[i], load_son, max_son);
            load += load_son;
            if (max < max_son) max = max_son;
        }
    }
    load += cities[node].load;
    if (max < load) max = load;
}

long long max_load()
{
    long long load, max;
    max_load(0, load, max);
    return max;
}

int main()
{
    size_t n, m;
    int city1, city2;
    long long cars;
    std::cin >> n;
    std::cin >> m;
    
    for (size_t i = 0; i < n - 1; ++i) {
        std::cin >> city1;
        std::cin >> city2;
        cities[city1].cmap.push_back(city2);
        cities[city2].cmap.push_back(city1);
    }
    
    if (debug) std::cout << "inicjalizacja drzewa\n";

    init_tree();
    
    if (debug) {
        std::cout << "debag przodków\n";
        for (size_t i = 0; i < n; ++i) {
            std::cout << i << ": ";
            for (size_t a = 0; a < cities[i].ancestor.size(); ++a)
                std::cout << cities[i].ancestor[a] << " ";
            std::cout << "\n";
        }
    }

    for (size_t i = 0; i < m; ++i) {
        if (debug) std::cout << "czytam " << i+1 << " wiersz obciążenia\n"; 
        std::cin >> city1;
        std::cin >> city2;
        std::cin >> cars;
        
        insert(city1, city2, cars);
        
        if (debug) std::cout << "wstawione\n";
        
        if (debug) {
            std::cout << "debag\n";
            for (size_t i = 0; i < n; ++i) {
                std::cout << i << ": " << "->" << cities[i].load << " ";
                std::cout << "\n";
            }
        }

    
    }
    
    std::cout << max_load() << std::endl;

    return 0;
}
